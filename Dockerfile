FROM tiangolo/uvicorn-gunicorn-fastapi:python3.11

# Install Poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

# Copy using poetry.lock* in case it doesn't exist yet
COPY ./pyproject.toml ./poetry.lock* /app/

RUN poetry install --no-root --only main

COPY ./app /app

# The Elastic Beanstalk CLI's local command requires exposed ports to be declared
EXPOSE 80