import logging
from enum import IntEnum
from typing import Optional, List, Tuple

from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import ORJSONResponse
from pydantic import BaseModel
from pydantic.fields import Field
from fitz import Document


class Metadata(BaseModel):
    format: str
    title: str
    author: str
    subject: str
    keywords: str
    creator: str
    producer: str
    creationDate: str
    modDate: str
    trapped: str
    encryption: Optional[str]


class HasBoundingBox(BaseModel):
    bbox: Tuple[float, float, float, float] = Field(
        description="The coordinates of two diagonally opposite points of a rectangle: x0, y0, x1, y1.",
    )


class BlockType(IntEnum):
    text = 0
    image = 1


class Block(HasBoundingBox):
    type: BlockType


class Span(HasBoundingBox):
    origin: Tuple[float, float] = Field(
        description="Coordinates of the first character's bottom left point."
    )
    font: str
    ascender: float
    descender: float
    size: float
    flags: int
    color: int
    text: str


class Line(HasBoundingBox):
    wmode: int
    dir: List[float] = Field(
        description="Unit vector that indicates the relative writing speed in x and y. Left-right if x is positive; right-left if x is negative. Top-bottom if y is positive; bottom-top if y is negative."
    )
    spans: List[Span]


class TextBlock(Block):
    type: BlockType = BlockType.text
    lines: List[Line]
    number: int = Field(description="BLock number 0-indexed.")


class Page(BaseModel):
    width: float = Field(description="Width in pixels.")
    height: float = Field(description="Height in pixels.")
    blocks: List[TextBlock]


class Extraction(BaseModel):
    filename: str
    content_type: str
    metadata: Metadata
    pages: List[Page]
    page_count: int


app = FastAPI(default_response_class=ORJSONResponse)


async def extract(file: UploadFile) -> Extraction:
    content_type: str = file.content_type
    filename: str = file.filename
    with Document(stream=await file.read(), filetype=content_type) as doc:
        pages = [Page(**page.get_textpage().extractDICT()) for page in doc]
        metadata = doc.metadata
        page_count = doc.page_count
    return Extraction(
        filename=filename,
        content_type=content_type,
        metadata=metadata,
        pages=pages,
        page_count=page_count,
    )


@app.post("/", response_model=Extraction)
async def extract_text(file: UploadFile = File(...)) -> Extraction:
    try:
        return await extract(file)
    except Exception as error:
        logging.error(str(error))
        raise HTTPException(status_code=400, detail="Unable to parse file.") from error
