# Introduction

PDF-Textractor is a web application with an API for extracting the text content of a PDF file in a tree structure with per-word boundary boxes built with [FastAPI](https://fastapi.tiangolo.com) and [PyMuPDF](https://pymupdf.readthedocs.io/en/latest/).

# Example

```
curl --location --request POST 'pdf-textractor.local' --form 'file=@"/path/to/my.pdf"'

{
    "filename": "my.pdf",
    "content_type": "application/pdf",
    "metadata": {
        "format": "PDF 1.4",
        "title": "",
        "author": "",
        "subject": "",
        "keywords": "",
        "creator": "",
        "producer": "",
        "creationDate": "D:20210201033304-08'00'",
        "modDate": "D:20210201033304-08'00'",
        "trapped": "",
        "encryption": null
    },
    "pages": [
        {
            "width": 2.0,
            "height": 2.0,
            "blocks": [
                {
                    "bbox": [
                        40.0,
                        62.04600143432617,
                        122.44600677490234,
                        119.76300048828125
                    ],
                    "type": 0,
                    "lines": [
                        {
                            "bbox": [
                                40.0,
                                62.04600143432617,
                                122.44600677490234,
                                81.32400512695312
                            ],
                            "wmode": 0,
                            "dir": [1.0, 0.0],
                            "spans": [
                                {
                                    "bbox": [
                                        40.0,
                                        62.04600143432617,
                                        122.44600677490234,
                                        81.32400512695312
                                    ],
                                    "origin": [40.0, 77.0260009765625],
                                    "font": "Helvetica-Bold",
                                    "ascender": 1.0700000524520874,
                                    "descender": -0.3070000112056732,
                                    "size": 14.0,
                                    "flags": 16,
                                    "color": 0,
                                    "text": "Hello World"
                                }
                            ]
                        },
                        ...
                    ]
                },
                ...
            ],
        }
        ...
    ],
    "page_count": 1
}
```

# Installation and Execution

## Without Docker

1. Clone this repository.
1. Ensure you have Python version 3.8 or higher installed:
    1. `python --version`
        1. If `python --version` is >= 3.8.0 then you're good to go.
    1. Install [pyenv](https://github.com/pyenv/pyenv) and follow its instructions for installing Python versions.
1. Install [Poetry](https://python-poetry.org/docs/#installation)
1. `poetry install`
1. `poetry run uvicorn --reload --port 8888 app.main:app`
1. Submit a post request to the application. E.g. `curl --location --request POST 'localhost:8888' --form 'file=@"/path/to/my.pdf"'`

## With Docker

1. Clone this repository.
1. `docker build -t myimage .`
1. `docker run -p 127.0.0.1:8888:80 --name mycontainer myimage`
1. Submit a post request to the application. E.g. `curl --location --request POST 'localhost:8888' --form 'file=@"/path/to/my.pdf"'`

# API documentation

The application contains automatically generated API documentation viewable via [Swagger UI](https://github.com/swagger-api/swagger-ui) or [ReDoc](https://github.com/Redocly/redoc). The Swagger UI version is available at `<pdf-textractor-host>:<port>/docs` (e.g. http://127.0.0.1:8888/docs) and the ReDoc version is available at `<pdf-textractor-host>:<port>/docs` (e.g. http://127.0.0.1:8888/redoc). A JSON [OpenAPI](https://www.openapis.org) document is available at `<pdf-textractor-host>:<port>/openapi.json` (e.g. http://127.0.0.1:8888/openapi.json).

# Development

## Without Docker

Modify files as desired. The `--reload` [option to uvicorn](https://www.uvicorn.org/#command-line-options) should cause it to reload the automatically reload the application on file changes. However, it may not catch dependency changes.

## With Docker

Run the container with the [live reload script](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker#development-live-reload) instead of the default command. E.g. `docker run -d -p 8888:80 -v $(pwd)/app:/app myimage /start-reload.sh`

# References

- [FastApi](https://fastapi.tiangolo.com)
- [uvicorn-gunicorn-fastapi-docker](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker#development-live-reload)
- [PyMuPDF](https://pymupdf.readthedocs.io/en/latest/)
- [MuPDF](https://mupdf.com)
- [Docker](https://www.docker.com)

# License

PDF-Textractor is distributed under GNU AFFERO GPL V3.0 (the same license as PyMuPDF and MuPDF), a copy of which is included in file LICENSE.md.
